<?php

namespace Drupal\admin_toolbar_content;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides an interface defining a MenuLinksPlugin.
 */
interface AdminToolbarContentPluginInterface {

  /**
   * Initialises the plugin.
   *
   * Creates new menu links based on the base definition of the deriver.
   *
   * @param array $links
   *   The links array.
   * @param mixed $base_plugin_link_definition
   *   The base plugin link definition.
   */
  public function initialize(array &$links, mixed $base_plugin_link_definition): void;

  /**
   * Create the links this plugin provides.
   */
  public function createMenuLinkItems(): void;

  /**
   * Alter discovered menu links.
   *
   * Allows a plugin to hook into the alteration of existing discovered menu
   * items.
   *
   * @param array $links
   *   The links array.
   */
  public function alterDiscoveredMenuLinks(array &$links): void;

  /**
   * Given an entity, determine if a menu rebuild needs to be triggered.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return bool
   *   Whether the menu needs to rebuild.
   */
  public function needsMenuLinkRebuild(EntityInterface $entity): bool;

  /**
   * Allows a plugin to hook into the menu preprocess.
   *
   * @param array $variables
   *   The variables array.
   */
  public function preprocessMenu(array &$variables): void;

  /**
   * Build a config form.
   *
   * Allows a plugin to add extra settings to the admin toolbar content
   * configuration settings form.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The form array.
   */
  public function buildConfigForm(array &$form, FormStateInterface $form_state): array;

}
