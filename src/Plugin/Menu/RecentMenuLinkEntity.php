<?php

namespace Drupal\admin_toolbar_content\Plugin\Menu;

use Drupal\admin_toolbar_tools\Plugin\Menu\MenuLinkEntity;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Menu\StaticMenuLinkOverridesInterface;

/**
 * Provides a menu link plugins for configuration entities.
 */
class RecentMenuLinkEntity extends MenuLinkEntity {

  /**
   * Constructs a new MenuLinkEntity.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Menu\StaticMenuLinkOverridesInterface $static_override
   *   The static override storage.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, StaticMenuLinkOverridesInterface $static_override, EntityTypeManagerInterface $entity_type_manager) {
    $storage = $entity_type_manager->getStorage($plugin_definition['metadata']['entity_type']);
    $type = $entity_type_manager->getDefinition($plugin_definition['metadata']['entity_type']);
    // Get the n-th entity (entity_count) this user is allowed to see.
    $id = current($storage->getQuery()
      ->condition($type->getKey('bundle'), $plugin_definition['metadata']['entity_bundle'])
      ->sort('changed', 'DESC')
      ->sort($type->getKey('id'), 'DESC')
      ->range($plugin_definition['metadata']['entity_count'], 1)
      ->accessCheck()
      ->execute()
    );

    // Make sure the correct entity is loaded in the parent call.
    $plugin_definition['metadata']['entity_id'] = $id ?: 0;

    // Override the placeholder in route parameters config with the real entity id.
    $plugin_definition['route_parameters'][$plugin_definition['metadata']['entity_type']] = $id ?: 0;

    parent::__construct($configuration, $plugin_id, $plugin_definition, $static_override, $entity_type_manager);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    $contexts = parent::getCacheContexts();
    $contexts[] = 'user';
    return $contexts;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    if (!$this->entity) {
      $description = (string) $this->pluginDefinition['description'];
    }
    return $description ?? parent::getDescription();
  }

}
