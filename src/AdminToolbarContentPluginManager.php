<?php

namespace Drupal\admin_toolbar_content;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Menu\MenuLinkManagerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\State\StateInterface;

/**
 * Manages admin toolbar content plugins.
 */
class AdminToolbarContentPluginManager extends DefaultPluginManager implements AdminToolbarContentPluginManagerInterface {

  /**
   * A Drupal State object.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * A Menu link plugin manager object.
   *
   * @var \Drupal\Core\Menu\MenuLinkManagerInterface
   */
  protected $menuLinkPluginManager;

  /**
   * Constructs a new AdminToolbarContentPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   An object that implements CacheBackendInterface.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   An object that implements ModuleHandlerInterface.
   * @param \Drupal\Core\State\StateInterface $state
   *   A Drupal State object.
   * @param \Drupal\Core\Menu\MenuLinkManagerInterface $menu_link_plugin_manager
   *   A Menu link plugin manager object.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, StateInterface $state, MenuLinkManagerInterface $menu_link_plugin_manager) {
    parent::__construct(
      'Plugin/AdminToolbarContent',
      $namespaces,
      $module_handler,
      'Drupal\admin_toolbar_content\AdminToolbarContentPluginInterface',
      'Drupal\admin_toolbar_content\Annotation\AdminToolbarContentPlugin'
    );
    $this->setCacheBackend($cache_backend, 'admin_toolbar_content_plugins');
    $this->alterInfo('admin_toolbar_content_plugins_info');
    $this->state = $state;
    $this->menuLinkPluginManager = $menu_link_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function menuLinkRebuild(EntityInterface $entity): void {

    if ($this->state->get('system.maintenance_mode')) {
      // We only re-generate the menu if we are not in maintenance mode.
      return;
    }

    $needsRebuild = FALSE;

    $plugins = $this->getDefinitions();

    foreach ($plugins as $plugin_id => $definition) {
      try {
        $plugin = $this->createInstance($plugin_id, $definition);
        if ($plugin->isEnabled() && $plugin->needsMenuLinkRebuild($entity)) {
          $needsRebuild = TRUE;
          break;
        }
      }
      catch (PluginException $e) {
        // Silently continue if plugin is not found.
      }
    }

    if ($needsRebuild) {
      $this->menuLinkPluginManager->rebuild();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function menuLinksDiscoveredAlter(array &$links): void {

    $plugins = $this->getDefinitions();

    foreach ($plugins as $plugin_id => $definition) {
      try {
        $plugin = $this->createInstance($plugin_id, $definition);
        if ($plugin->isEnabled()) {
          $plugin->alterDiscoveredMenuLinks($links);
        }
      }
      catch (PluginException $e) {
        // Silently continue if plugin is not found.
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function preprocessMenu(array &$variables): void {

    $plugins = $this->getDefinitions();

    foreach ($plugins as $plugin_id => $definition) {
      try {
        $plugin = $this->createInstance($plugin_id, $definition);
        if ($plugin->isEnabled()) {
          $plugin->preprocessMenu($variables);
        }
      }
      catch (PluginException $e) {
        // Silently continue if plugin is not found.
      }
    }
  }

}
